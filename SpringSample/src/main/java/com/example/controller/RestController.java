package com.example.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.example.form.IndexForm;

@org.springframework.web.bind.annotation.RestController
@Scope("request")
@RequestMapping("rest")
public class RestController {
	@GetMapping("getjson")
	@ResponseBody
	public List<IndexForm> getJson() {
		List<IndexForm> result = new ArrayList<IndexForm>();
		result.add(new IndexForm("hoge", 1, new Date()));
		result.add(new IndexForm("fuga", 2, new Date()));
		result.add(new IndexForm("piyo", 3, new Date()));
		return result;
	}

	@GetMapping("getboolean/{value}")
	@ResponseBody
	public Boolean getBoolean(@PathVariable String value) {
		if (Character.isDigit(value.charAt(0)) == false) {
			return false;
		}
		return Integer.parseInt(value) == 1 + 1;
	}

	@GetMapping("getboolean")
	@ResponseBody
	public Boolean getBooleanParam(@RequestParam("value") String value) {
		if (Character.isDigit(value.charAt(0)) == false) {
			return false;
		}
		return Integer.parseInt(value) == 1 + 1;
	}
}
