package com.example.form;

import java.util.Date;

public class IndexForm {
	private String sampleString = new String();
	private Integer sampleInteger = Integer.valueOf(0);
	private Date sampleDate = new Date();

	public IndexForm(String sampleString, Integer sampleInteger, Date sampleDate) {
		super();
		this.sampleString = sampleString;
		this.sampleInteger = sampleInteger;
		this.sampleDate = sampleDate;
	}

	public String getSampleString() {
		return sampleString;
	}

	public void setSampleString(String sampleString) {
		this.sampleString = sampleString;
	}

	public Integer getSampleInteger() {
		return sampleInteger;
	}

	public void setSampleInteger(Integer sampleInteger) {
		this.sampleInteger = sampleInteger;
	}

	public Date getSampleDate() {
		return sampleDate;
	}

	public void setSampleDate(Date sampleDate) {
		this.sampleDate = sampleDate;
	}

}
