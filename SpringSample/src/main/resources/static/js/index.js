/**
 * 
 */
function getJson() {
	fetch('/getjson')
		.then(response => response.json())
		.then(data => {
			document.getElementById('getJsonResult').innerText = JSON.stringify(data)
		})
}
/**
 * 
 */
function getBoolean(value) {
	fetch(`/getboolean/${value}`)
		.then(response => {
			return response.json();
		})
		.then(data => {
			console.log(data);
			if (data === true) {
				console.log('true');
			} else if (data === false) {
				console.log('false');
			} else {
				console.log('undefined');
			}
			document.getElementById('getBooleanResult').innerText = JSON.stringify(data)
		}).catch()
}
/**
 * 
 */
function getParam(value) {
	fetch(`/getboolean?value=${value}`)
		.then(response => {
			return response.json();
		})
		.then(data => {
			console.log(data);
			if (data === true) {
				console.log('true');
			} else if (data === false) {
				console.log('false');
			} else {
				console.log('undefined');
			}
			document.getElementById('getParamResult').innerText = JSON.stringify(data)
		}).catch()
}
/**
 * 
 */
function getRestJson() {
	fetch('/rest/getjson')
		.then(response => response.json())
		.then(data => {
			document.getElementById('getRestJsonResult').innerText = JSON.stringify(data)
		})
}
/**
 * 
 */
function getRestBoolean(value) {
	fetch(`/rest/getboolean/${value}`)
		.then(response => {
			return response.json();
		})
		.then(data => {
			console.log(data);
			if (data === true) {
				console.log('true');
			} else if (data === false) {
				console.log('false');
			} else {
				console.log('undefined');
			}
			document.getElementById('getRestBooleanResult').innerText = JSON.stringify(data)
		}).catch()
}
/**
 * 
 */
function getRestParam(value) {
	fetch(`/getboolean?value=${value}`)
		.then(response => {
			return response.json();
		})
		.then(data => {
			console.log(data);
			if (data === true) {
				console.log('true');
			} else if (data === false) {
				console.log('false');
			} else {
				console.log('undefined');
			}
			document.getElementById('getRestParamResult').innerText = JSON.stringify(data)
		}).catch()
}